`timescale 1ns / 1ps

module Controller(B_cond,cond_sel,clk,reset,IR ,ldPC,ldIR,ldMAR,ldSP,ldAC,ldMDR, tPC,tIR,tMAR,tSP,tAC,tMDR,tConst,wr_reg,rd_reg,rd_mem,wr_mem,s, fsel,state,next_state,write_addr,read_addr);
 
        input B_cond;
        output [3:0] cond_sel;
        reg [3:0] cond_sel;
        input clk,reset;
        output [0:6] state;
        input [15:0] IR;
        output reg ldPC,ldIR,ldMAR,ldSP,ldAC,ldMDR, tPC,tIR,tMAR,tSP,tAC,tMDR,tConst,wr_reg,rd_reg,rd_mem,wr_mem,s;
        output[3:0] fsel;
        output [2:0] write_addr,read_addr;
        wire [2:0] write_addr,read_addr;
        reg [2:0] reg_addr;
        output[0:6] next_state;
        reg [3:0] fsel;
        reg[0:6] next_state;
        reg[0:6] state;

        initial
        begin
            state=7'b0;
            next_state=7'b0;
            ldPC=0;
            ldIR=0;
            ldMAR=0;
            ldSP=0;
            ldAC=0;
            ldMDR=0;
            tPC=0;
            tIR=0;
            tMAR=0;
            tSP=0;
            tAC=0;
            tMDR=0;
            tConst=0;
            wr_reg=0;
            rd_reg=0;
            rd_mem=0;
            wr_mem=0;
            s=0;
        end
 
        
         always@(posedge clk)
                   begin
                   casex(state)
                      0:
                                          begin
                                              $display("state = ",state);
                                              $display("initialize");
                                              if(reset)
                                              begin
                                                  //
                                                  next_state=state;
                                              end
                                              else
                                                  next_state=1;
                                          end
                                          1:
                                          begin
                                              $display("state = ",state);
                                              $display("MAR<-PC");
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                   
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=1;
                   
                                              fsel=4'b0111;
                   
                                              ldMAR=1;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                              next_state=13;
                                          end
                                          13:
                                          begin
                   
                                               $display("state = ",state);
                                               $display("IR<-<MAR>");
                                               s=0;
                                               ldPC=0;
                   
                                               ldSP=0;
                                               ldAC=0;
                                               ldMDR=0;
                                               tPC=0;
                                               ldMAR=0;
                                               tIR=0;
                                               
                                               tSP=0;
                                               tAC=0;
                                               tMDR=0;
                                               tConst=0;
                                               wr_reg=0;
                                               rd_reg=0;
                                               wr_mem=0;
                                               tMAR=1;
                                               rd_mem=1;
                                               
                                               ldIR=1;
                                               next_state=14;
                                          end
                                          14:
                                          begin
                                               $display("state = ",state);
                                               $display("PC<-PC+1");
                                               s=0;
                                                ldPC=0;
                                               ldIR=0;
                   
                                               ldSP=0;
                                               ldAC=0;
                                               ldMDR=0;
                                               tPC=0;
                                               ldMAR=0;
                                               tIR=0;
                                               tMAR=0;
                                               tSP=0;
                                               tAC=0;
                                               tMDR=0;
                                               tConst=0;
                                               wr_reg=0;
                                               rd_reg=0;
                                               rd_mem=0;
                                               wr_mem=0;
                                               //$display("1");
                   
                                               tPC=1;
                                               //$display("2");
                                               //#1;
                                               fsel=4'b1000;
                                               //$display("3");
                                               ldPC=1;
                                               //$display("4");
                                               if(IR[15:12]==15)
                                               begin
                                                  if(IR[11:10]==3)
                                                   begin
                                                     next_state=2;
                                                   end
                                                  else if(IR[11:10]==0 || IR[11:10]==1)
                                                  begin
                                                      next_state=3;
                                                   end
                   
                                                end
                                                else if(IR[15:12]==12)
                                                begin
                                                   next_state=5;
                                                end
                                                else
                                                begin
                                                   cond_sel=IR[15:12];
                                                    if(B_cond)
                                                    begin
                                                       next_state=4;
                                                    end
                                                    else
                                                    begin
                                                       //next_state=1;
                                                       next_state=1;
                                                    end
                                                end
                   
                                          end
                                          2:
                                          begin
                                              $display("state = ",state);
                                              $display("SP<-SP-1");
                                              s=0;
                                               ldPC=0;
                                              ldIR=0;
                   
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                              tSP=1;
                                              fsel=1001;
                                              ldSP=1;
                                              next_state=21;
                                          end
                                          21:
                                          begin
                                               $display("state = ",state);
                                               $display("MAR<-SP");
                                               s=0;
                                                ldPC=0;
                                               ldIR=0;
                   
                                               ldSP=0;
                                               ldAC=0;
                                               ldMDR=0;
                                               tPC=0;
                                               ldMAR=0;
                                               tIR=0;
                                               tMAR=0;
                                               tSP=0;
                                               tAC=0;
                                               tMDR=0;
                                               tConst=0;
                                               wr_reg=0;
                                               rd_reg=0;
                                               rd_mem=0;
                                               wr_mem=0;
                   
                                               tSP=1;
                                               fsel=4'b0111;
                                               ldMAR=1;
                                               next_state=22;
                                          end
                                          22:
                                          begin
                                              $display("state = ",state);
                                              $display("MDR<-RegBank[IR[12:15]]");
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                             //  tIR=1;
                                             //  fsel=4'b0110;
                                             //  rd_reg=1;
                                              reg_addr=IR[2:0];
                                              rd_reg=1;
                                              fsel=4'b0111;   //transX
                                              s=0;
                                              ldMDR=1;
                   
                                              next_state=23;
                   
                                          end
                                          23:
                                          begin
                                               $display("state = ",state);
                                               $display("<MAR><-MDR");
                                               s=0;
                                               ldPC=0;
                                               ldIR=0;
                                               ldSP=0;
                                               ldAC=0;
                                               ldMDR=0;
                                               tPC=0;
                                               ldMAR=0;
                                               tIR=0;
                                               tMAR=0;
                                               tSP=0;
                                               tAC=0;
                                               tMDR=0;
                                               tConst=0;
                                               wr_reg=0;
                                               rd_reg=0;
                                               rd_mem=0;
                                               wr_mem=0;
                                               tMAR=1;
                                               s=1;
                                               tMDR=1;
                                               
                                               wr_mem=1;
                   
                                               next_state=1;
                                          end
                                          3:
                                          begin
                                              $display("state = ",state);
                                              $display("MAR <- SP");
                   
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                              tSP=1;
                                              fsel=4'b0111;
                                              ldMAR=1;
                                              next_state=31;
                   
                                          end
                                          31:
                                          begin
                   
                                              $display("state = ",state);
                                              $display("MDR <- <MAR>");
                   
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                                              s=1;
                                              tMAR=1;
                                              rd_mem=1;
                                              
                                              ldMDR=1;
                                              next_state=32;
                                          end
                                          32:
                                          begin
                                              $display("state = ",state);
                                              $display("SP<-SP+1");
                   
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                              tSP=1;
                                              fsel=4'b1000;
                                              ldSP=1;
                   
                                              if (IR[11:10]==0)
                                              begin
                                                 if (IR[9:8]==1)
                                                     next_state=6;
                                                 else if(IR[9:8]==2)
                                                      next_state=7;
                                                 else if (IR[9:8]==3)
                                                      next_state=8;
                   
                                              end
                                              else if(IR[11:10]==1)
                                              begin
                                                   if (IR[9:8]==2)
                                                       next_state=9;
                                                   else if(IR[9:8]==0)
                                                       next_state=10;
                                                   else if (IR[9:8]==1)
                                                       next_state=11;
                                              end
                                          end
                                          4:
                                          begin
                                              $display("state = ",state);
                                              $display("AC<-IR[4:15]");
                   
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                                              //TODO: Check AC<-ir[4:15]
                   
                                             //  tIR=1;
                                             //  fsel=4'b0101;
                                              //TODO: Need a new temporary reg to hold Z so that it can be transferred to AC
                   
                                              next_state=41;
                                          end
                                          41:
                                          begin
                                              $display("state = ",state);
                                              $display("PC<-PC+AC");
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                              //TODO: check logic
                                              tPC=1;
                                              tAC=1;
                                              fsel=4'b0000;
                                              ldPC=1;
                                             next_state=1;
                                          end
                                          5:
                                          begin
                                              $display("state = ",state);
                                              $display("SP<_SP-1");
                   
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                              tSP=1;
                                              fsel=4'b1001;
                                              ldSP=1;
                                              next_state=51;
                                          end
                                          51:
                                          begin
                                              $display("state = ",state);
                                              $display("MAR<-SP");
                   
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                              tSP=1;
                                              fsel=4'b0111;
                                              ldMAR=1;
                                              next_state=52;
                                          end
                                          52:
                                          begin
                                              $display("state = ",state);
                                              $display("MDR<-<MAR>");
                   
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                                              tMAR=1;
                                              rd_mem=1;
                                              s=1;
                                              ldMDR=1;
                                              next_state=53;
                                          end
                                          53:
                                          begin
                                              $display("state = ",state);
                                              $display("PC<-MDR");
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                   
                                              tMDR=1;
                                              fsel=4'b0111;
                                              ldPC=1;
                                              next_state=1;
                                          end
                                          6:
                                           begin
                                               $display("state = ",state);
                                               $display("RegBank[IR[12:15]] <- MDR");
                                               s=0;
                                               ldPC=0;
                                               ldIR=0;
                                               ldSP=0;
                                               ldAC=0;
                                               ldMDR=0;
                                               tPC=0;
                                               ldMAR=0;
                                               tIR=0;
                                               tMAR=0;
                                               tSP=0;
                                               tAC=0;
                                               tMDR=0;
                                               tConst=0;
                                               wr_reg=0;
                                               rd_reg=0;
                                               rd_mem=0;
                                               wr_mem=0;
                   
                                               tMDR=1;
                                               fsel=4'b0111;
                                               reg_addr=IR[2:0];
                                               wr_reg=1;
                                               next_state=1;
                                           end
                                         7:
                                            begin
                                                $display("state = ",state);
                                                $display("AC<-MDR");
                   
                                                s=0;
                                                ldPC=0;
                                                ldIR=0;
                                                ldSP=0;
                                                ldAC=0;
                                                ldMDR=0;
                                                tPC=0;
                                                ldMAR=0;
                                                tIR=0;
                                                tMAR=0;
                                                tSP=0;
                                                tAC=0;
                                                tMDR=0;
                                                tConst=0;
                                                wr_reg=0;
                                                rd_reg=0;
                                                rd_mem=0;
                                                wr_mem=0;
                   
                                                tMDR=1;
                                                ldAC=1;
                   
                                                next_state=71;
                                            end
                                         71:
                                          begin
                                              $display("state = ",state);
                                              $display("RegBank[IR[12:15]]<-AC+RegBank[IR[12:15]]");
                                              s=0;
                                              ldPC=0;
                                              ldIR=0;
                                              ldSP=0;
                                              ldAC=0;
                                              ldMDR=0;
                                              tPC=0;
                                              ldMAR=0;
                                              tIR=0;
                                              tMAR=0;
                                              tSP=0;
                                              tAC=0;
                                              tMDR=0;
                                              tConst=0;
                                              wr_reg=0;
                                              rd_reg=0;
                                              rd_mem=0;
                                              wr_mem=0;
                   
                   
                   
                                              reg_addr=IR[2:0];
                                              rd_reg=1;
                                              tAC=1;
                                              fsel=4'b000;
                                              reg_addr=IR[2:0];
                                              wr_reg=1;
                                              next_state=1;
                                          end
                   
                                         8:
                                             begin
                                                 $display("state = ",state);
                                                 $display("RegBank[IR[12:15]]<- ~MDR");
                   
                                                 s=0;
                                                 ldPC=0;
                                                 ldIR=0;
                                                 ldSP=0;
                                                 ldAC=0;
                                                 ldMDR=0;
                                                 tPC=0;
                                                 ldMAR=0;
                                                 tIR=0;
                                                 tMAR=0;
                                                 tSP=0;
                                                 tAC=0;
                                                 tMDR=0;
                                                 tConst=0;
                                                 wr_reg=0;
                                                 rd_reg=0;
                                                 rd_mem=0;
                                                 wr_mem=0;
                   
                                                 //TODO;
                                                 next_state=1;
                                             end
                                         9:
                                              begin
                                                  $display("state = ",state);
                                                  $display("PC <- MDR");
                   
                                                  s=0;
                                                  ldPC=0;
                                                  ldIR=0;
                                                  ldSP=0;
                                                  ldAC=0;
                                                  ldMDR=0;
                                                  tPC=0;
                                                  ldMAR=0;
                                                  tIR=0;
                                                  tMAR=0;
                                                  tSP=0;
                                                  tAC=0;
                                                  tMDR=0;
                                                  tConst=0;
                                                  wr_reg=0;
                                                  rd_reg=0;
                                                  rd_mem=0;
                                                  wr_mem=0;
                   
                                                  tMDR=1;
                                                  fsel=4'b0111;
                                                  ldPC=1;
                                                  next_state=1;
                                              end
                                      10:
                                               begin
                                                   $display("state = ",state);
                                                   $display("AC<-MDR");
                   
                                                   s=0;
                                                   ldPC=0;
                                                   ldIR=0;
                                                   ldSP=0;
                                                   ldAC=0;
                                                   ldMDR=0;
                                                   tPC=0;
                                                   ldMAR=0;
                                                   tIR=0;
                                                   tMAR=0;
                                                   tSP=0;
                                                   tAC=0;
                                                   tMDR=0;
                                                   tConst=0;
                                                   wr_reg=0;
                                                   rd_reg=0;
                                                   rd_mem=0;
                                                   wr_mem=0;
                   
                                                   tMDR=1;
                                                   ldAC=1;
                   
                                                   next_state=101;
                                               end
                                      101:
                                               begin
                                                  $display("state = ",state);
                                                  $display("RegBank[IR[12:15]]<- AC | RegBank[IR[12:15]]");
                   
                                                  s=0;
                                                  ldPC=0;
                                                  ldIR=0;
                                                  ldSP=0;
                                                  ldAC=0;
                                                  ldMDR=0;
                                                  tPC=0;
                                                  ldMAR=0;
                                                  tIR=0;
                                                  tMAR=0;
                                                  tSP=0;
                                                  tAC=0;
                                                  tMDR=0;
                                                  tConst=0;
                                                  wr_reg=0;
                                                  rd_reg=0;
                                                  rd_mem=0;
                                                  wr_mem=0;
                                                  //TODO;
                   
                                                  tAC=1;
                                                  next_state=1;
                                               end
                                     11:
                                                begin
                                                    $display("state = ",state);
                                                    $display("RegBank[IR[12:15]]<- ~MDR");
                   
                                                    s=0;
                                                    ldPC=0;
                                                    ldIR=0;
                                                    ldSP=0;
                                                    ldAC=0;
                                                    ldMDR=0;
                                                    tPC=0;
                                                    ldMAR=0;
                                                    tIR=0;
                                                    tMAR=0;
                                                    tSP=0;
                                                    tAC=0;
                                                    tMDR=0;
                                                    tConst=0;
                                                    wr_reg=0;
                                                    rd_reg=0;
                                                    rd_mem=0;
                                                    wr_mem=0;
                   
                                                    //TODO
                                                    fsel=4'b0100; //negation
                                                    next_state=1;
                                                end
                   
                                     12:
                                          begin
                                              if (reset)
                                                  next_state=0;
                                              else
                                              next_state=1;
                                          end
                   endcase
                   state=next_state;
               end
               assign write_addr=reg_addr;
               assign read_addr=reg_addr;
           
endmodule
