`timescale 1ns / 1ps


module CPU_datapath(clk,reset);
	
//	module CPU_datapath(clk,ldPC,ldIR,ldMAR,ldSP,ldAC,ldMDR, tPC,tIR,tMAR,tSP,tAC,tMDR,tConst,wr_reg,rd_reg,s,rd_mem,wr_mem,fsel,C,V,S,Z_det,state,next_state,reset,XY_bus,Z_bus,
//        ,write_addr,read_addr,r0,r1,r2,r3,r4,r5,r6,r7);


	wire[15:0] out_data,MAR,MDRin,memory_in,tmp,reg_rdOut,Y;
	//reg ldAC,tPC,tIR,tMAR,tSP,tAC,tMDR,tConst;
	//input ldAC,tPC,tIR,tMAR,tSP,tAC,tMDR,tConst;
	wire ldAC,tPC,tIR,tMAR,tSP,tAC,tMDR,tConst;
	input clk,reset;
	//reg ldPC,ldIR,ldSP,ldMAR,rd_mem,wr_mem,ldMDR,wr_reg,rd_reg,s;
	//input ldPC,ldIR,ldSP,ldMAR,rd_mem,wr_mem,ldMDR,wr_reg,rd_reg,s;
	wire ldPC,ldIR,ldSP,ldMAR,rd_mem,wr_mem,ldMDR,wr_reg,rd_reg,s;
    //reg[3:0] fsel;
	//input [3:0] fsel;
	wire[3:0] fsel;
	wire[2:0] write_addr,read_addr;
	//reg[0:6] next_state;
    wire [0:6] next_state;
    //reg B_cond;
    wire B_cond;
    //reg[3:0] cc;
    wire [3:0] cc;
    //reg [15:0] AC;
	wire[15:0] AC;
	//reg[0:6] state;
	wire [0:6] state;
	wire C,V,S,Z_det;
	//reg[15:0] r0,r1,r2,r3,r4,r5,r6,r7;
    wire[15:0] r0,r1,r2,r3,r4,r5,r6,r7;
	//input [15:0] XY_bus,Z_bus;
	wire [15:0] XY_bus,Z_bus;

    //assign XY_bus=16'b0;
    //assign Z_bus=16'b0;
	
	Register_dp Program_Counter(Z_bus,clk,XY_bus,ldPC,tPC,reset);
	Register_sp Stack_Pointer(Z_bus,clk,XY_bus,ldSP,tSP,reset);
	Register_dp Instruction_Register(out_data,clk,XY_bus,ldIR,tIR,reset);
	Register_dp Mem_Addr_Reg(Z_bus,clk,MAR,ldMAR,tMAR,reset);
	Register_dp reg_AC(XY_bus,clk,AC,ldAC,tAC,reset);
	//Register_constant Const_1(clk,XY_bus,tConst);
 
	Register_MDR Mem_Data_Reg(out_data,Z_bus,clk, memory_in,XY_bus,ldMDR,tMDR,reset,s);     
	memory mem(MAR, memory_in,out_data,rd_mem,wr_mem);
	reg_bank registers(Z_bus,write_addr,read_addr,wr_reg,rd_reg,XY_bus,clk,reset,r0,r1,r2,r3,r4,r5,r6,r7);
	ALU alu(XY_bus,AC,Z_bus,fsel,C,V,S,Z_det,clk);
	StatusDetection stat_det(B_cond,C,V,S,Z_det,cc,clk,reset);
	Controller controller(B_cond,cc,clk,reset,Instruction_Register.reg_data,ldPC,ldIR,ldMAR,ldSP,ldAC,ldMDR, tPC,tIR,tMAR,tSP,tAC,tMDR,tConst,wr_reg,rd_reg,rd_mem,wr_mem,s, fsel,state,next_state,write_addr,read_addr); 
    //assign XY_bus=Program_Counter.reg_data;

//	always@(negedge clk)
//	begin
//		$display("\n state=%h PC=%h MAR=%h MDR=%h ldMDR=%h out_data=%h XY_bus=%h fsel=%h Z_bus=%h r0=%h r1=%h r2=%h r3=%h r4=%h\n",state,PC,MAR,MDR,ldMDR,out_data,,XY_bus,fsel,Z_bus,r0,r1,r2,r3,r4);
//	   $display(" state=",state," IR",Instruction_Register.reg_data,"\n");
//	   $display(" PC= ",Program_Counter.reg_data);
//	   $display(" MAR= ",Mem_Addr_Reg.reg_data);
//	   $display(" tPC= ",tPC);
//	   $display(" Z_bus = ",Z_bus);
//	   $display(" XY_bus = ",XY_bus);
//	end

endmodule


module reg_bank(data,wp,pa,wrr,rdr,p,clk,rst,r0,r1,r2,r3,r4,r5,r6,r7);
	input[15:0] data;
	input[2:0] wp,pa;
	input wrr,rdr;
	input clk,rst;
	output[15:0] p;
	wire [15:0] p;
	wire[7:0] wr;
	output[15:0] r0,r1,r2,r3,r4,r5,r6,r7;
	wire [15:0] r0,r1,r2,r3,r4,r5,r6,r7;
	decoder3to8 write(wp,wr,wrr);
	
	Register load0(data,clk,r0,wr[0],rdr,rst);
	Register load1(data,clk,r1,wr[1],rdr,rst);
	Register load2(data,clk,r2,wr[2],rdr,rst);
	Register load3(data,clk,r3,wr[3],rdr,rst);
	Register load4(data,clk,r4,wr[4],rdr,rst);
	Register load5(data,clk,r5,wr[5],rdr,rst);
	Register load6(data,clk,r6,wr[6],rdr,rst);
	Register load7(data,clk,r7,wr[7],rdr,rst);
	
	mux8to1 read(p,pa,rdr,r0,r1,r2,r3,r4,r5,r6,r7);

endmodule

module decoder3to8(in,wr,ld);
	input[2:0] in;
	input ld;
	output[7:0] wr;

	reg[7:0] out;
	initial
	begin
	   out=8'b00000001;
	end
	always@(in or ld)
	begin
		if(ld)
		begin
			casex(in)
				3'b000:out=8'b00000001;
				3'b001:out=8'b00000010;
				3'b010:out=8'b00000100;
				3'b011:out=8'b00001000;
				3'b100:out=8'b00010000;
				3'b101:out=8'b00100000;
				3'b110:out=8'b01000000;
				3'b111:out=8'b10000000;
			endcase
		end
		else out=8'b00000000;
	end
	
	assign wr=out;
endmodule


module Register(in,clk,out,ld,treg,rst);
	//input[15:0] in;
	input [15:0] in;
	wire [15:0] in;
	input treg;
	wire treg;
	
	input clk,ld,rst;
	output[15:0] out;
	wire [15:0] out;
	reg[15:0] reg_data;
	initial
	begin
	   reg_data=16'h4444;
	end
	//always@(negedge clk)
	always@ (negedge clk)
	begin
		if(rst)
		  begin
		      reg_data=16'b0;
		  end
		else 
		begin
			if(ld)
			begin
			 reg_data=in;
			 $display("reading register - value = ",reg_data," in =",in);
			end
		end
	end // end always
	
	assign out = (treg)?reg_data:16'hZZZZ;
	

	
endmodule




module Mux2to1(input wire a,input wire b,input wire sel,output wire out);
	wire internal[1:0];
	and( internal[1] , a , sel );
	and( internal[0] , b , ~sel );
	or( out , internal[1] , internal[0] );
endmodule

module Mux4to1(input wire [3:0] a,input wire [1:0] sel,output wire out);
	wire internal[1:0];
	Mux2to1 m1 (a[3],a[2],sel[0],internal[1]);
	Mux2to1 m2 (a[1],a[0],sel[0],internal[0]);
	Mux2to1 m3 (internal[1],internal[0],sel[1],out);
endmodule

module MULTIPLEXER8(input wire [7:0] a,input wire [2:0] sel,output wire out);
	wire internal[1:0];
	Mux4to1 m1 (a[7:4],sel[1:0],internal[1]);
	Mux4to1 m2 (a[3:0],sel[1:0],internal[0]);
	Mux2to1 m3 (internal[1],internal[0],sel[2],out);
endmodule




module Register_MDR (from_mem,in_z,clk,to_mem,out_xy,ld,treg,rst,s);           //////////////~
	input[15:0] from_mem,in_z;
	input treg,s;
	input clk,ld,rst;
	output[15:0] to_mem,out_xy;
    wire [15:0] to_mem;
    wire [15:0] out_xy;
	reg[15:0] out;
	reg[15:0] reg_data;
	initial
	begin
	   reg_data=16'b0;
	end
	always@(negedge clk)// or posedge rst)
	begin
		if(rst)
		  begin
		      reg_data=16'b0;
		      out=16'b0;
		  end
		else if (ld)
		  begin
		      if (s)
		      begin
		          reg_data=from_mem;
		      end
		      else
		      begin
		          reg_data=in_z;
		      end
		  end
	end
	
	assign to_mem=(s&treg)?reg_data:(16'hZZZZ);
	assign out_xy=(treg&(~s))?reg_data:(16'hZZZZ);
endmodule

/*
module Register_constant(clk,out,treg);
	
	input treg;
	wire wrr;
	
	input clk;
	output[15:0] out;
	wire [15:0] out;
	reg[15:0] reg_data;
	initial
	begin
	   reg_data=16'b0000000000000001;
	end
	
   assign out = (treg)?reg_data:16'hZZZZ;
        
	
endmodule
*/








module Register_sp(in,clk,out,ld,treg,rst);
	input[15:0] in;
	input treg;
	wire [15:0] in;
	
	input clk,ld,rst;
	output[15:0] out;
	wire [15:0] out;
	reg[15:0] reg_data;
	initial
	begin
	   reg_data=16'b0000000000001111;
	end
	always@(negedge clk)// or posedge rst)
	begin
		if(rst)
		  begin
		      reg_data=16'b0;
		      //out=16'b0;
		  end
		else begin
			if(ld)
			begin
			 reg_data=in;
			 
			end
		end
	end
	assign out = (treg)?reg_data:16'hZZZZ;
    
	
endmodule

module Register_dp(in,clk,out,ld,treg,rst);
	//input[15:0] in;
	input [15:0] in;
	wire [15:0] in;
	input treg;
	wire treg;
	
	input clk,ld,rst;
	output[15:0] out;
	wire [15:0] out;
	reg[15:0] reg_data;
	initial
	begin
	   reg_data=16'b0;
	end
	//always@(negedge clk)
	always@ (negedge clk)
	begin
		if(rst)
		  begin
		      reg_data=16'b0;
		  end
		else 
		begin
			if(ld)
			begin
			 reg_data=in;
			 $display("HELLO ld",in," data",reg_data," \n");
			end
		end
	end // end always
	
	assign out = (treg)?reg_data:16'hZZZZ;
	
	/*
                else if(treg)
                begin
                 out=reg_data;
                 $display("HELLO treg",out," data",reg_data," \n");
                end
                else begin
                o
                end
                */
	
endmodule




/*
module decoder3to8(in,out);
	input[2:0] in;
	output[7:0] out;
	wire[7:0] w1,w2;
	
	or(w1[0],in[0],in[1]);
	or(w2[0],w1[0],in[2]);
	not(out[0],w2[0]);
	
	nor(w1[1],in[1],in[2]);
	and(out[1],w1[1],in[0]);
	
	nor(w1[2],in[2],in[0]);
	and(out[2],w1[2],in[1]);
	
	and(w1[3],in[0],in[1]);
	not(w2[3],in[2]);
	and(out[3],w1[3],w2[3]);

	nor(w1[4],in[1],in[0]);
	and(out[4],w1[4],in[2]);
	
	and(w1[5],in[2],in[0]);
	not(w2[5],in[1]);
	and(out[5],w2[5],w1[5]);
	
	and(w1[6],in[2],in[1]);
	not(w2[6],in[0]);
	and(out[6],w2[6],w1[6]);
	
	and(w1[7],in[0],in[1]);
	and(out[7],w1[7],in[2]);

endmodule
*/


module mux8to1(out,sel,ld,r0,r1,r2,r3,r4,r5,r6,r7);
	output[15:0] out;
	input[2:0] sel;
	input ld;
	input[15:0] r0,r1,r2,r3,r4,r5,r6,r7;
	wire [15:0] out;
	reg [15:0] tmp;
	always@(sel or ld or r0 or r1 or r2 or r3 or r4 or r5 or r6 or r7)
	begin
		if(ld)begin
			casex(sel)
				3'b000:tmp=r0;
				3'b001:tmp=r1;
				3'b010:tmp=r2;
				3'b011:tmp=r3;
				3'b100:tmp=r4;
				3'b101:tmp=r5;
				3'b110:tmp=r6;
				3'b111:tmp=r7;
			endcase
		end
		
	end
	assign out = (ld)?tmp:16'hZZZZ;
endmodule

