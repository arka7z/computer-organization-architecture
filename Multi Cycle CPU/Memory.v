`timescale 1ns / 1ps


module memory (Address,D_in,D_out,RD,WR); 
  parameter RAM_DEPTH = 1 << 16;
 
  input [15:0] Address ;
  wire [15:0] Address ;
  input  [15:0] D_in ;
  wire  [15:0] D_in ;
  output reg [15:0] D_out=0;
  input wire WR;
  input wire RD; 

  reg [15:0] mem [0:RAM_DEPTH-1];
  initial  
  begin
  //Give instructions to the memory here
    mem[0]=16'b1111110000110001;
    mem[1]=16'b1111001000000001;
    mem[2]=16'b1111001100000001;
    mem[12]=16'b0001000100010001;
    mem[13]=16'b0001000100010001;
    mem[11]=16'b0001000100010001;
    mem[14]=16'b0001000100010001;
  end
    
  always @ * 
  begin //: MEM_WRITE
     if (WR) 
     begin
        $display("D_In = ", D_in);
         mem[Address] = D_in;
        $display("mem[address] = ", mem[Address] , "Address = ", Address);
     end
     if (RD) 
     begin
         D_out = mem[Address];
         $display(" D_out = ", D_out, "mem[address] = ",mem[Address]);
     end
  end
  endmodule 