`timescale 1ns / 1ps

module DFF(q,q_not,d,clk,rst);
output q,q_not;
input wire d,clk,rst;
reg q,q_not;
 initial
    begin
        q=1'b0; q_not=1'b1;
    end
    always@ (posedge clk)
       begin
       if (rst)
       begin
        q=1'b0; q_not=1'b1;
       end
       else
        begin
         q=d;
         q_not=~d;
        end 
       end
endmodule