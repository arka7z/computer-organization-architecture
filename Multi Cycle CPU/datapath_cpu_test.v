`timescale 1ns / 1ps


module CPU_dp_TB;

  reg clk,reset;

	//reg [15:0] r0,r1,r2,r3,r4,r5,r6,r7;
	// Instantiate the Unit Under Test (UUT)
	wire ldAC,ldPC,ldIR,ldSP,ldMAR,rd_mem,wr_mem,ldMDR,wr_reg,rd_reg,s,tPC,tIR,tMAR,tSP,tAC,tMDR,tConst;
	wire [15:0] XY_bus,Z_bus;	
	wire [3:0] fsel;
	wire [0:6] state;
	CPU_datapath uut ( 
		.clk(clk), 
        .reset(reset)
	);

	wire [15:0] PC_val,r0,r1,r2,r3,r4,r5,r6,r7,MAR_bus ; 
	assign PC_val = uut.Program_Counter.reg_data;
	wire [15:0] IR_val , MAR_val , MDR_val;
	wire [15:0] mem_in_data,mem_out_data,sp_val;
	wire [7:0] decoded_reg;
	assign decoded_reg=uut.registers.wr;
	assign MAR_bus= uut.MAR;
	assign MDR_val = uut.Mem_Data_Reg.reg_data;
	assign s=uut.s;
    assign mem_in_data=uut.mem.D_in;
    assign mem_out_data=uut.mem.D_out;
	assign IR_val = uut.Instruction_Register.reg_data;
	assign MAR_val = uut.Mem_Addr_Reg.reg_data;
	assign sp_val = uut.Stack_Pointer.reg_data;
	//   assign ldAC(ldAC),
    assign ldPC=uut.ldPC;
    assign tPC=uut.tPC;
    assign ldMAR=uut.ldMAR;
    assign tMAR = uut.tMAR;
    assign state= uut.state;
    assign ldSP = uut.ldSP;
    assign tSP = uut.tSP;
    assign ldMDR = uut.ldMDR;
    assign tAC = uut.tAC;    
    assign ldAC = uut.ldAC;
    assign rd_mem= uut.rd_mem;
    assign wr_mem= uut.wr_mem;
    assign rd_reg=uut.rd_reg;
    assign wr_reg=uut.wr_reg;
    assign ldIR=uut.ldIR;
    assign tIR=uut.tIR;
    assign tMDR = uut.tMDR;
    assign r0=uut.registers.load0.reg_data;
    assign r1=uut.registers.load1.reg_data;
    assign r2=uut.registers.load2.reg_data;
    assign r3=uut.registers.load3.reg_data;
    assign r4=uut.registers.load4.reg_data;
    assign r5=uut.registers.load5.reg_data;
    assign r6=uut.registers.load6.reg_data;
    assign r7=uut.registers.load7.reg_data;
    /*
    assign ldIR(ldIR),
    assign ldSP(ldSP),
    .ldMAR(ldMAR),
    .rd_mem(rd_mem),
    .wr_mem(wr_mem),
    .ldMDR(ldMDR),
    .wr_reg(wr_reg),
    .rd_reg(rd_reg),
    .s(s),
    .tPC(tPC),
    .tIR(tIR),
    .tMAR(tMAR),
    .tSP(tSP),
    .tAC(tAC),
    .tMDR(tMDR),
    .tConst(tConst)
    */
	assign XY_bus = uut.XY_bus;                            
	assign Z_bus = uut.Z_bus;
	assign fsel = uut.fsel;

    integer i=0,j=0,sp_set=0,reg_set=0;
	initial
	begin
		// Initialize Inputs
		clk = 0;
        reset=1;
        i=0;
        sp_set=0;
        reg_set=0;
		#10;
       reset=0;
        
	end

	always
	begin
	   #1;
	   if(i<60)
	       begin
	           if(reset!=1 && sp_set!=1)
	           begin
	               uut.Stack_Pointer.reg_data=16'h000c;
	               sp_set=1;
	           end
	           clk=~clk;
	           i=i+1;
	           $display(" mem_in_data = ",uut.mem.D_in);
	           $display(" sp = ",uut.Stack_Pointer.reg_data);
	           
	           $display(" memory[0008]= ",uut.mem.mem[8]);
	                 
	           $display(" memory[000a]= ",uut.mem.mem[10]);
	           
	           $display(" memory[0009]= ",uut.mem.mem[9]);
	           
	           $display(" memory[000b]= ",uut.mem.mem[11]);
	           $display(" reg2 = ", uut.registers.load2.reg_data);
	       end
	   if(reset!=1 && reg_set!=1)
	   begin
	       uut.registers.load0.reg_data=16'h0001;
	       uut.registers.load1.reg_data=16'h0001;
	       uut.registers.load2.reg_data=16'h0001;
	       uut.registers.load3.reg_data=16'h0001;
	       uut.registers.load4.reg_data=16'h0001;
           uut.registers.load5.reg_data=16'h0001;
           uut.registers.load6.reg_data=16'h0001;
           uut.registers.load7.reg_data=16'h0001;
           reg_set=1;
	   end
	   #1;
	end
      
endmodule
