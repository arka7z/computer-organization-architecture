`timescale 1ns / 1ps

module ALU (X,Y,Z,fsel,C,V,S,Z_det,clk);
    
    output [15:0]X,Y;
    //input [15:0]X,Y;
    wire signed [15:0]X,Y;
    output [15:0] Z;
    wire signed [15:0] Z;
    output C,V,S,Z_det;
    wire V,S,Z_det,tmp;
    reg C;
    input clk;
    input [3:0] fsel;
    
    reg [15:0] sum_tmp;
    reg [15:0] t2;
    initial
    begin
        t2=16'h0000;
    end
    
    // reg[15:0] x_tmp,y_tmp;
    
    /*
        Funcitons to be implemented:
        add:
        sub
        neg
        or 
        not
        labeladd
        treg
        transX
        incX
    */
    
    always @(*)// or posedge clk)
    begin
       casex(fsel)
           4'b0000:
                    begin
                        t2=X+Y;
                        {C,sum_tmp}={1'b0,X}+{1'b0,Y};
                    end
           4'b0001:
                    t2=X-Y;
           4'b0010:
                   t2=-X;
           4'b0011:
                   t2=X|Y;
           4'b0100:
                   t2=~X;
           4'b0101:
                    t2=Y+{4'b0,X[11:0]};
           4'b0110:
                   t2={12'b0,X[3:0]};
           4'b0111:
                   begin
                        
                        t2=X;    
                        
//                         $display("X in Transx= ",X);
//                         $display("Y in Transx = ",Y);
//                         $display("Z in Transx = ",Z);                    
                   end
           4'b1000:
                   begin
                   
                    t2=X+1;
//                  $display("X in INC = ",X);
//                  $display("Y in INC = ",Y);
//                  $display("Z in INC = ",Z);
                                     
                  end
            4'b1001:
                    begin
                    t2=X-1;
                    end
          default:
                    begin
                        t2 = 0;
                    end
          
       endcase 
          end
           
     
     assign Z=t2;
     //assign Z=(fsel==8)?(X+1):16'b0;//(X+16'h0001):16'b0;
     
     assign tmp=Z[0]|Z[1]|Z[2]|Z[3]|Z[4]|Z[5]|Z[6]|Z[7]|Z[8]|Z[9]|Z[10]|Z[11]|Z[12]|Z[13]|Z[14]|Z[15];
     assign Z_det=~tmp;
     assign S=Z[15];
     assign V=(((X[15]&Y[15])^(~sum_tmp[15])) | (((~X[15])& (~Y[15]))^(sum_tmp[15])));
 endmodule
 
 
 
 