`timescale 10ps / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.10.2017 10:46:59
// Design Name: 
// Module Name: controller
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module controller(   C,V,S,Z_det,clk,reset,IR,fsel,state,next_state);
        
        input C,V,S,Z_det,clk,reset;
        input[0:6] state;
        input [0:15] IR;
        output[0:3] fsel;
        output[0:6] next_state;
        reg [0:3] fsel;
        reg[0:6] next_state;
        
        always@(state or reset)
            begin
            casex(state)
                0:
                begin
                    $display("state = ",state);
                    $display("initialize");
                    if(reset)
                        next_state=state;
                    else
                        next_state=1;
                end
                1:
                begin
                    $display("state = ",state);
                    $display("MAR<-PC");
                    next_state=13;
                end
                13:
                begin
                     $display("state = ",state);
                     $display("IR<-<MAR>");
                     next_state=14;
                end
                14:
                begin
                     $display("state = ",state);
                     $display("PC<-PC+1");
                     if(IR[0:3]==15)
                     begin
                        if(IR[4:5]==3)
                         begin
                           next_state=2;
                         end
                        else if(IR[4:5]==0 || IR[4:5]==1)
                        begin
                            next_state=3;
                         end
                        
                      end
                      else if(IR[0:3]==12)
                      begin
                         next_state=5;
                       end
                       else
                       begin
                          if(C | V | S | Z_det)
                          begin
                             next_state=4;
                          end
                          else
                          begin
                             next_state=12;
                          end
                      end
                     
                end
                2:
                begin
                    $display("state = ",state);
                    $display("SP<-SP-1");
                    next_state=21;
                end
                21:
                begin
                     $display("state = ",state);
                     $display("MAR<-SP");
                     next_state=22;
                end
                22:
                begin
                    $display("state = ",state);
                    $display("MDR<-RegBank[IR[12:15]]");
                    next_state=23;
                  
                end
                23:
                begin
                     $display("state = ",state);
                     $display("<MAR><-MDR");
                     next_state=12;
                end
                3:
                begin
                    $display("state = ",state);
                    $display("MAR <- SP");
                    next_state=31;
                             
                end
                31:
                begin
                        
                    $display("state = ",state);
                    $display("MDR <- <MAR>");
                    next_state=32;
                end
                32:
                begin
                    $display("state = ",state);
                    $display("SP<-SP+1");
                    if (IR[4:5]==0)
                    begin
                       if (IR[6:7]==1)
                           next_state=6;
                       else if(IR[6:7]==2)
                            next_state=7;
                       else if (IR[6:7]==3)
                            next_state=8;
                                             
                    end
                    else if(IR[4:5]==1)
                    begin
                         if (IR[6:7]==2)
                             next_state=9;
                         else if(IR[6:7]==0)
                             next_state=10;
                         else if (IR[6:7]==1)
                             next_state=11;  
                    end      
                end
                4:
                begin
                    $display("state = ",state);
                    $display("AC<-IR[4:15]");
                    next_state=41;
                end
                41:
                begin
                    $display("state = ",state);
                    $display("PC<-PC+AC");
                   next_state=12;  
                end
                5:
                begin
                    $display("state = ",state);
                    $display("SP<_SP-1");
                    next_state=51;
                end
                51:
                begin
                    $display("state = ",state);
                    $display("MAR<-SP");
                    next_state=52;
                end
                52:
                begin
                    $display("state = ",state);
                    $display("MDR<-<MAR>");
                    next_state=53;
                end
                53:
                begin
                    $display("state = ",state);
                    $display("PC<-MDR");
                    next_state=12;
                end
                6:
                 begin
                     $display("state = ",state);
                     $display("RegBank[IR[12:15]] <- MDR");
                     next_state=12;
                 end
               7:
                  begin
                      $display("state = ",state);
                      $display("AC<-MDR");
                      next_state=71;
                  end
               71:
                begin
                    $display("state = ",state);
                    $display("RegBank[IR[12:15]]<-AC+RegBank[IR[12:15]]");
                    next_state=12;
                end
               
               8:
                   begin
                       $display("state = ",state);
                       $display("RegBank[IR[12:15]]<- ~MDR");
                       next_state=12;
                   end
               9:
                    begin
                        $display("state = ",state);
                        $display("PC <- MDR");
                        next_state=12;
                    end
            10:
                     begin
                         $display("state = ",state);
                         $display("AC<-MDR");
                         next_state=101;
                     end
            101:
                     begin
                        $display("state = ",state);
                        $display("RegBank[IR[12:15]]<- AC | RegBank[IR[12:15]]");
                        next_state=12;
                     end
           11:
                      begin
                          $display("state = ",state);
                          $display("RegBank[IR[12:15]]<- ~MDR");
                          next_state=12;
                      end

           12:
                begin
                    if (reset)
                        next_state=0;
                    else
                    next_state=12;
                end
            endcase
        end
    
endmodule
