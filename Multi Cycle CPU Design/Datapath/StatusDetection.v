`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 

//module MULTIPLEXER8(input wire [7:0] a,input wire [2:0] sel,output wire out); 
//////////////////////////////////////////////////////////////////////////////////


module StatusDetection(B_cond,C,V,S,Z_det,cc,clk,reset);

    output reg B_cond;
    input wire C,V,S,Z_det,clk,reset;
    input[3:0] cc;
    wire[3:0] cc;
    
    reg[7:0] flags;
    DFF C_flag(flags[0],flags[1],C,clk,reset);
    DFF V_flag(flags[2],flags[3],V,clk,reset);
    DFF S_flag(flags[4],flags[5],S,clk,reset);
    DFF Z_det_flag(flags[6],flags[7],Z_det,clk,reset);
    
    MUX9to1  status_sel(B_cond,cc,flags);
    
endmodule
