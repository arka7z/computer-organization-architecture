`timescale 1ns / 1ps



module MUX9to1(out,sel,r);
	output out;
	input[3:0] sel;
	input[7:0] r;
	reg out;
	always@(sel  or r[0] or r[1] or r[2] or r[3] or r[4] or r[5] or r[6] or r[7])
	begin
            casex(sel)
				4'b0000:out=r[0];
				4'b0001:out=r[1];
				4'b0010:out=r[2];
				4'b0011:out=r[3];
				4'b0100:out=r[4];
				4'b0101:out=r[5];
				4'b0110:out=r[6];
				4'b0111:out=r[7];
				4'b1000:out=1'b1;
			endcase
	end
endmodule
