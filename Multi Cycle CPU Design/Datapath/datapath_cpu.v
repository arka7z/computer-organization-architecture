
module CPU_datapath(clk,ldPC,ldIR,ldMAR,ldSP,ldAC,ldMDR, tPC,tIR,tMAR,tSP,tAC,tMDR,tConst,wr_reg,rd_reg,rd_mem,wr_mem,fsel,C,V,S,Z_det,state,next_state,reset,XY_bus,Z_bus,
	,write_addr,read_addr,r0,r1,r2,r3,r4,r5,r6,r7);


	wire[15:0] out_data,MAR,MDRin,memory_in,tmp,reg_rdOut,Y;
	input ldAC,tPC,tIR,tMAR,tSP,tAC,tMDR,tConst;
	input clk,reset,ldPC,ldIR,ldSP,ldMAR,rd_mem,wr_mem,ldMDR,wr_reg,rd_reg;
	input[2:0] fsel,write_addr,read_addr;
	input[6:0] next_state;

	output[6:0] state;
	output C,V,S,Z_det;
	output[15:0] r0,r1,r2,r3,r4,r5,r6,r7,XY_bus,Z_bus;
	Register_dp Program_Counter(Z_bus,clk,XY_bus,ldPC,tPC,reset);
	Register_dp Instruction_Register(out_data,clk,XY_bus,ldIR,tIR,reset);
	Register_dp Mem_Addr_Reg(Z_bus,clk,MAR,ldMAR,tMAR,reset);
	Register_dp Const_1(clk,XY_bus,tConst);
	Register_MDR Mem_Data_Reg(out_data,Z_bus,clk,XY_bus,ldMDR,tMDR,reset);        //////////////~
	memory mem(MAR, memory_in,out_data,rd_mem,wr_mem);
	reg_bank registers(Z_bus,write_addr,read_addr,wr_reg,rd_reg,XY_bus,clk,reset,r0,r1,r2,r3,r4,r5,r6,r7);
	
	always@(clk&(~reset))
	begin
		$display("\n state=%h PC=%h MAR=%h MDR=%h ldMDR=%h out_data=%h  XY_bus=%h fsel=%h Z_bus=%h r0=%h r1=%h r2=%h r3=%h r4=%h\n",state,PC,MAR,MDR,ldMDR,out_data,,XY_bus,fsel,Z_bus,r0,r1,r2,r3,r4);
	end

endmodule


module reg_bank(data,wp,pa,wrr,rdr,p,clk,rst,r0,r1,r2,r3,r4,r5,r6,r7);
	input[15:0] data;
	input[2:0] wp,pa;
	input wrr,rdr;
	input clk,rst;
	output[15:0] p;
	
	wire[7:0] wr;
	output[15:0] r0,r1,r2,r3,r4,r5,r6,r7;
	
	decoder3to8 write(wp,wr);
	
	Register load0(data,clk,r0,wr[0],wrr,rst);
	Register load1(data,clk,r1,wr[1],wrr,rst);
	Register load2(data,clk,r2,wr[2],wrr,rst);
	Register load3(data,clk,r3,wr[3],wrr,rst);
	Register load4(data,clk,r4,wr[4],wrr,rst);
	Register load5(data,clk,r5,wr[5],wrr,rst);
	Register load6(data,clk,r6,wr[6],wrr,rst);
	Register load7(data,clk,r7,wr[7],wrr,rst);
	
	mux8to1 read(p,pa,rdr,r0,r1,r2,r3,r4,r5,r6,r7);

endmodule






module Register_MDR (in_mem,in_z,clk,out,ld,treg,rst,s);           //////////////~
	input[15:0] in_mem,in_z;
	input treg,s;
	input clk,ld,rst;
	output[15:0] out;
	reg[15:0] out;
	reg[15:0] reg_data;
	initial
	begin
	   reg_data=16'b0;
	end
	always@(posedge clk or posedge rst)
	begin
		if(rst)
		  begin
		      reg_data=16'b0;
		      out=16'b0;
		  end
		else if (ld)
		  begin
		      if ( s)
		      begin
		          reg_data=in_mem;
		      end
		      else
		      begin
		          reg_data=in_z;
		      end
		  end
		else if(treg)
		  begin
		      out=reg_data;
		  end
	end
endmodule


module Register_constant(clk,out,treg);
	
	input treg;
	wire wrr;
	
	input clk;
	output[15:0] out;
	reg[15:0] out;
	reg[15:0] reg_data;
	initial
	begin
	   reg_data=16'b0000000000000001;
	end
	always@(posedge clk or posedge treg)
	begin
		if(treg) 
		begin
		  out=reg_data;
	    end
	end
endmodule



module Register(in,clk,out,ld,wrr,rst);
	input[15:0] in;
	input wrr;
	wire wrr;
	input clk,ld,rst;
	output[15:0] out;
	reg[15:0] out;
	always@(posedge clk or posedge rst)
	begin
		if(rst)
		  out=16'b0;
		else begin
			if(ld && wrr)
			 out=in;
			else 
			 out=out;
		end
	end
endmodule


module Register_dp(in,clk,out,ld,treg,rst);
	input[15:0] in;
	input treg;
	wire wrr;
	
	input clk,ld,rst;
	output[15:0] out;
	reg[15:0] out;
	reg[15:0] reg_data;
	initial
	begin
	   reg_data=16'b0;
	end
	always@(posedge clk or posedge rst)
	begin
		if(rst)
		  begin
		      reg_data=16'b0;
		      out=16'b0;
		  end
		else begin
			if(ld)
			begin
			 reg_data=in;
			 
			end
			else if(treg)
			begin 
			 out=reg_data;
			end
		end
	end
endmodule



module decoder3to8(in,out);
	input[2:0] in;
	output[7:0] out;
	wire[7:0] w1,w2;
	
	or(w1[0],in[0],in[1]);
	or(w2[0],w1[0],in[2]);
	not(out[0],w2[0]);
	
	nor(w1[1],in[1],in[2]);
	and(out[1],w1[1],in[0]);
	
	nor(w1[2],in[2],in[0]);
	and(out[2],w1[2],in[1]);
	
	and(w1[3],in[0],in[1]);
	not(w2[3],in[2]);
	and(out[3],w1[3],w2[3]);

	nor(w1[4],in[1],in[0]);
	and(out[4],w1[4],in[2]);
	
	and(w1[5],in[2],in[0]);
	not(w2[5],in[1]);
	and(out[5],w2[5],w1[5]);
	
	and(w1[6],in[2],in[1]);
	not(w2[6],in[0]);
	and(out[6],w2[6],w1[6]);
	
	and(w1[7],in[0],in[1]);
	and(out[7],w1[7],in[2]);

endmodule



module mux8to1(out,sel,ld,r0,r1,r2,r3,r4,r5,r6,r7);
	output[15:0] out;
	input[2:0] sel;
	input ld;
	input[15:0] r0,r1,r2,r3,r4,r5,r6,r7;
	reg[15:0] out;
	always@(sel or ld or r0 or r1 or r2 or r3 or r4 or r5 or r6 or r7)
	begin
		if(ld)begin
			casex(sel)
				3'b000:out=r0;
				3'b001:out=r1;
				3'b010:out=r2;
				3'b011:out=r3;
				3'b100:out=r4;
				3'b101:out=r5;
				3'b110:out=r6;
				3'b111:out=r7;
			endcase
		end
		else out=out;
	end
endmodule

