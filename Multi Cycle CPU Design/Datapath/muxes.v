`timescale 1ns / 1ps

module Mux2to1(input wire a,input wire b,input wire sel,output wire out);
	wire internal[1:0];
	and( internal[1] , a , sel );
	and( internal[0] , b , ~sel );
	or( out , internal[1] , internal[0] );
endmodule

module Mux4to1(input wire [3:0] a,input wire [1:0] sel,output wire out);
	wire internal[1:0];
	Mux2to1 m1 (a[3],a[2],sel[0],internal[1]);
	Mux2to1 m2 (a[1],a[0],sel[0],internal[0]);
	Mux2to1 m3 (internal[1],internal[0],sel[1],out);
endmodule

module MULTIPLEXER8(input wire [7:0] a,input wire [2:0] sel,output wire out);
	wire internal[1:0];
	Mux4to1 m1 (a[7:4],sel[1:0],internal[1]);
	Mux4to1 m2 (a[3:0],sel[1:0],internal[0]);
	Mux2to1 m3 (internal[1],internal[0],sel[2],out);
endmodule