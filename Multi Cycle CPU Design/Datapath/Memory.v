module memory (Address,D_in,D_out,RD,WR); 
  parameter RAM_DEPTH = 1 << 16;
 
  input [15:0] Address ;
  input  [15:0] D_in ;
  output reg [15:0] D_out=0;
  input  WR;
  input RD; 

  reg [15:0] mem [0:RAM_DEPTH-1];
  initial  
  begin
  //Give instructions to the memory here
  end
    
  always @ * 
  begin : MEM_WRITE
     if (WR) 
     begin
         mem[Address] = D_in;
     end
     if (RD) 
     begin
         D_out = mem[Address];
     end
  end
  endmodule 