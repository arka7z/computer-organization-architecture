`timescale 1ns / 1ps


module CPU_dp_TB;

  reg clk,ldPC,ldIR,ldMAR,ldSP,ldAC,ldMDR, tPC,tIR,tMAR,tSP,tAC,tMDR,tConst,wr_reg,rd_reg,rd_mem,wr_mem;
  reg [2:0] fsel;
  reg C,V,S,Z_det;
  reg[6:0] state,next_state;
  reg reset;
  reg [15:0] XY_bus,Z_bus;
  
	reg [2:0] write_addr,read_addr;
	reg [16:0] r0,r1,r2,r3,r4,r5,r6,r7;
	// Instantiate the Unit Under Test (UUT)
	CPU_datapath uut ( 
		.clk(clk), 
		.ldPC(ldPC), 
		.ldIR(ldIR), 
		.ldMAR(ldMAR), 
		.ldSP(ldSP),
		.ldAC(ldAC), 
        .ldMDR(ldMDR),
		.tPC(tPC),
        .tIR(tIR),
        .tMAR(tMAR),
        .tSP(tSP),
        .tAC(tAC),
        .tMDR(tMDR),
        .tConst(tConst),

		.wr_reg(wr_reg), 
		.rd_reg(rd_reg), 

		.rd_mem(rd_mem), 
		.wr_mem(wr_mem), 
		
 
		.fsel(fsel), 

		.C(C), 
		.V(V), 
		.S(S), 
		.Z_det(Z_det), 
		.state(state), 
		.next_state(next_state), 

		.reset(reset), 
        .XY_bus(XY_bus),
        .Z_bus(Z_bus),
        
        .write_addr(write_addr),
        .read_addr(read_addr),
		.r0(r0), 
		.r1(r1), 
		.r2(r2), 
		.r3(r3), 
		.r4(r4), 
		.r5(r5), 
		.r6(r6), 
		.r7(r7)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		ldPC = 0;
		ldIR = 0;
		ldMAR = 0;
		rd_mem = 0;
		wr_mem = 0;

		ldMDR=0;
		wr_reg = 0;
		rd_reg = 0;

		fsel = 0;
		next_state = 0;
		reset = 0;
		

		#100;
        
	end
      
endmodule
