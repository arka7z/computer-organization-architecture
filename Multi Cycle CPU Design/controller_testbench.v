`timescale 10ps / 1ps
module controller_test_bench();
    

        // Inputs
        reg C;
        reg V;
        reg S;
        reg Z_det;
        reg clk;
        reg reset;
        reg [0:6] state;
        reg [0:15] memory [0:255];
        reg [0:15] IR;
        // Outputs
        wire ldPC;
        wire ldIR;
        wire ldMAR;
        wire rd_mem;
        wire wr_mem;
        wire ldSP;
        wire ldMDR;
        wire ldAC;
        wire wr_reg;
        wire rd_reg;
        wire [0:3] fsel;
        wire [0:6] next_state;
        integer i=0;

        // Instantiate the Unit Under Test (UUT)
        controller uut 
        (
            .C(C), 
            .V(V), 
            .S(S), 
            .Z_det(Z_det), 
            .clk(clk), 
            .reset(reset),
            .IR(IR),

            .fsel(fsel), 
            .state(state), 
            .next_state(next_state)
        );
    
        initial begin
            // Initialize Inputs
            C = 0;
            V = 0;
            S = 0;
            Z_det = 0;
            clk = 0;
            reset = 1;
            IR = 16'b1111001100000000;
            state = 0;
            // Wait 100 ns for global reset to finish
            #50;
          reset=0;

        end
        
        always #1 clk=~clk;

        always@(posedge clk)
            state=next_state;

        always@state begin
             if(state==12)
                   begin
                       #2;
                       C = 0;
                       V = 0;
                       S = 0;
                       Z_det = 0;
                       $display("halt");
                       reset = 1;
                       i=i+1;
                       if(i==1)
                           IR = 16'b1111010000000000;
                       else if(i==2)
                       begin
                           C=1;
                           IR = 16'b0000000000000000;
                       end
                       else
                           $finish;
                       state = 0;
                       // Wait 100 ns for global reset to finish
                       #10;
                       reset=0;
                       // Add stimulus here
                                   
                   end

        end

endmodule
